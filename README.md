# User manual

## Getting started

Clone this repository:

```
git clone https://bitbucket.org/nhweston/comp335-visual.git
```

## Usage

### Generating a visualisation

In a command-line tool, navigate to `./bin`. Run `./simvis.sh <schd-exec> <config-file> <algorithm-id> [-n]` where:

- `<schd-exec>` is the path to your scheduler executable or class file.
- `<config-file>` is the path to the configuration file to run the simulator with.
- `<algorithm-id>` is the ID of the algorithm to run your scheduler with.
- `-n` is set if the simulator should be run in new-line mode.

Note that file paths may as usual be given relative to the directory containing `simvis.sh`.

An example invocation:

```
./simvis.sh ../../my-scheduler/schd.sh ../../my-scheduler/config.xml ff -n
```

This will generate a visualisation for the scheduler output on `config.xml` using the first-fit algorithm, with the simulator running in new-line mode.

### Using the visualisation

Once the visualisation has been generated, it can be viewed by opening `./app/index.html` in a modern web browser.

This visualisation shows the state of all server instances over the course of the server simulation. It shows jobs queued and running on a server, including the amount of resources consumed by a particular job. Use the buttons in the top-left to navigate backwards and forwards in time. You can also type in a particular step to jump to.

The progression of time can be automated using the options in the top-right. If "By Step" is selected, the progression is proportion to number of steps. The "Speed" field specifies how many steps to show per second. If "By Time" is selected, the progression will be proportional to system time. In this case, the "Speed" field specifies how many times faster the progression should be to the actual system time.
