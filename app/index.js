'use strict';

let step = -1;
let clock = undefined;

const SYSTEM = function () {
    let result = [];
    for (let type of DATA_SYSTEM) {
        let instances = [];
        for (let i=0; i<DATA_SYSTEM.length; i++) {
            instances[i] = new Server(type.id + '__' + i, type.numCores, type.memory, type.disk);
        }
        result[type.id] = instances;
    }
    return result;
} ();

const JOBS = function () {
    let result = [];
    for (let job of DATA_JOBS) {
        result[job.id] = new Job(job.id, job.numCores, job.memory, job.disk);
    }
    return result;
} ();

const verbs = {
    'SCHEDULED': 'assigned to',
    'RUNNING': 'running on',
    'COMPLETED': 'completed on'
};

function prev (n) {
    if (n <= 0 || step < 0) {
        displayInfo (DATA_LOG[step]);
        return;
    }
    const event = DATA_LOG[step];
    const job = JOBS[event.jobId];
    const instance = SYSTEM[event.typeId][event.instanceId];
    switch (event.state) {
        case 'SCHEDULED':   instance.undoEnqueue(job);      break;
        case 'RUNNING':     instance.undoRun(job);          break;
        case 'COMPLETED':   instance.undoTerminate(job);    break;
    }
    step--;
    prev(n-1);
}

function next (n) {
    if (n <= 0 || step >= DATA_LOG.length-1) {
        displayInfo (DATA_LOG[step]);
        return;
    }
    step++;
    const event = DATA_LOG[step];
    const job = JOBS[event.jobId];
    const instance = SYSTEM[event.typeId][event.instanceId];
    switch (event.state) {
        case 'SCHEDULED':   instance.enqueue(job);      break;
        case 'RUNNING':     instance.run(job);          break;
        case 'COMPLETED':   instance.terminate(job);    break;
    }
    next(n-1);
}

function setStep () {
    const elem = document.getElementById('step-field');
    let value = +(elem.innerText);
    if (value < -1) {
        value = -1;
    }
    else if (value >= DATA_LOG.length) {
        value = DATA_LOG.length -1;
    }
    elem.innerText = value;
    if (value < step) while (value < step) {
        prev();
    }
    else if (value > step) while (value > step) {
        next();
    }
}

function setTime (time) {
    if (step >= DATA_LOG.length-1) return;
    let current = DATA_LOG[step+1].time;
    while (time >= current && step < DATA_LOG.length) {
        next(1);
        if (step === DATA_LOG.length - 1) return;
        current = DATA_LOG[step+1].time;
    }
}

function startAnimation () {
    const speed = +(document.getElementById('animate-speed').value);
    if (speed < 1) return;
    if (document.getElementById('animate-by-time').checked) {
        const frameDuration = 50;
        const stepsPerFrame = 1000 / (20 * speed);
        let time = (step >= 0) ? DATA_LOG[step].time : 0;
        clock = setInterval (
            () => {
                time = time + stepsPerFrame;
                setTime(time);
                if (step === DATA_LOG.length - 1) {
                    clearInterval(clock)
                }
            },
            frameDuration
        )
    }
    else {
        const frameDuration = function () {
            if (speed < 20) return 1000 / speed;
            else return 50;
        } ();
        const stepsPerFrame = function () {
            if (speed < 20) return 1;
            else return speed / 20;
        } ();
        clock = setInterval (
            () => {
                next(stepsPerFrame);
                if (step === DATA_LOG.length - 1) {
                    clearInterval(clock);
                }
            },
            frameDuration
        )
    }
}

function stopAnimation () {
    if (clock !== undefined) {
        clearInterval(clock);
    }
    clock = undefined;
}

function displayInfo (event) {
    const elem = document.getElementById('info');
    if (event === undefined) {
        elem.innerText = '';
        return;
    }
    elem.innerHTML = `[${event.time}] Job ${event.jobId} ${verbs[event.state]} ${event.typeId} ${event.instanceId}`;
    document.getElementById('step-field').value = step;
}
