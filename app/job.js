'use strict';

const JobState = {
    PRE: 0,
    QUEUED: 1,
    ACTIVE: 2,
    POST: 3
};

class Job {

    id;
    cores; memory; disk;
    color;
    state;

    constructor (id, cores, memory, disk) {
        this.id = id;
        this.cores = cores;
        this.memory = memory;
        this.disk = disk;
        this.state = JobState.PRE;
    }

    setColor (color) {
        this.color = color;
    }

    setState (state) {
        this.state = state;
    }

}
