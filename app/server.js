'use strict';

class Server {

    id;
    cores; memory; disk;
    jobsActive;
    jobsQueued;
    colorCounts;

    constructor (id, cores, memory, disk) {
        this.id = id;
        this.cores = cores;
        this.memory = memory;
        this.disk = disk;
        this.jobsActive = [];
        this.jobsQueued = [];
        this.colorCounts = COLORS.map(() => 0);
    }

    addToActive (job) {
        this.jobsActive.push(job);
        this.jobsActive.sort(j => j.id);
        this.render();
    }

    addToQueued (job) {
        this.jobsQueued.push(job);
        this.jobsQueued.sort(j => j.id);
        this.render();
    }

    removeFromActive (job) {
        this.jobsActive = this.jobsActive.filter(j => j.id !== job.id);
        this.render();
    }

    removeFromQueued (job) {
        this.jobsQueued = this.jobsQueued.filter(j => j.id !== job.id);
        this.render();
    }

    enqueue (job) {
        if (job.color === undefined) {
            this.assignColor(job);
        }
        this.addToQueued(job);
        this.colorCounts[job.color] = this.colorCounts[job.color] + 1;
    }

    undoEnqueue (job) {
        this.removeFromQueued(job);
        this.colorCounts[job.color] = this.colorCounts[job.color] - 1;
    }

    run (job) {
        this.removeFromQueued(job);
        this.addToActive(job);
    }

    undoRun (job) {
        this.removeFromActive(job);
        this.addToQueued(job);
    }

    terminate (job) {
        this.removeFromActive(job);
        this.colorCounts[job.color] = this.colorCounts[job.color] - 1;
    }

    undoTerminate (job) {
        this.addToActive(job);
        this.colorCounts[job.color] = this.colorCounts[job.color] + 1;
    }

    render () {
        this.renderCores();
        this.renderMemory();
        this.renderDisk();
        this.renderQueue();
        this.renderActive();
    }

    renderCores () {
        let cumulative = 0;
        this.jobsActive.forEach (
            job => {
                for (let i=0; i<job.cores; i++) {
                    const elem = document.getElementById('core__' + this.id + '__' + cumulative);
                    elem.style.backgroundColor = COLORS[job.color];
                    cumulative++;
                }
            }
        );
        for (;;) {
            const elem = document.getElementById('core__' + this.id + '__' + cumulative);
            if (elem === null) break;
            else elem.style.backgroundColor = '#CCC';
            cumulative++;
        }
    }

    renderBar (name, value, scale) {
        let cumulative = 0;
        let bar = 0;
        for (let i=0; true; i++) {
            const elem = document.getElementById(name + '__' + this.id + '__' + i);
            if (elem === null) break;
            else elem.innerHTML = '';
        }
        this.jobsActive.forEach (
            job => {
                let width = Math.trunc(value(job) / (100 * scale));
                while (width > 0) {
                    if (width === 160) {
                        bar++;
                        cumulative = 0;
                    }
                    else if (cumulative + width < 160) {
                        cumulative += width;
                        const elem = document.getElementById(name + '__' + this.id + '__' + bar);
                        elem.innerHTML += `<div style="display: inline-block; width: ${width}px; height: 12px; background-color: ${COLORS[job.color]}"></div>`;
                        width = 0;
                    }
                    else {
                        width -= 160 - cumulative;
                        const elem = document.getElementById(name + '__' + this.id + '__' + bar);
                        elem.innerHTML += `<div style="=display: inline-block; width: ${160 - cumulative}px; height: 12px; background-color: ${COLORS[job.color]}"></div>`;
                        cumulative = 0;
                        bar++;
                    }
                }
            }
        )
    }

    renderActive () {
        const elem = document.getElementById('active__' + this.id);
        elem.innerHTML = '';
        for (let job of this.jobsActive) {
            elem.innerHTML = elem.innerHTML + `<span style="background-color: ${COLORS[job.color]}">${job.id}</span><span>&nbsp;</span>`;
        }
    }

    renderQueue () {
        const elem = document.getElementById('queue__' + this.id);
        elem.innerHTML = '';
        for (let job of this.jobsQueued) {
            elem.innerHTML = elem.innerHTML + `<span style="display: block"><span style="background-color: ${COLORS[job.color]}">${job.id}</span> ${job.cores} ${job.memory} ${job.disk}</span>`;
        }
    }

    renderMemory () {
        this.renderBar('mem', j => j.memory, 1);
    }

    renderDisk () {
        this.renderBar('disk', j => j.disk, 4);
    }

    assignColor (job) {
        let indexMin = undefined;
        let countMin = undefined;
        for (let i=0; i<COLORS.length; i++) {
            if (this.colorCounts[i] === 0) {
                job.setColor(i);
                return;
            }
            if (countMin === undefined || this.colorCounts[i] < countMin) {
                indexMin = i;
                countMin = this.colorCounts[i];
            }
        }
        job.setColor(indexMin);
    }

}
