rm simvis_fat.jar
rm simvis.jar

cd ..
sbt assembly
cd bin

if [ "$1" == "-s" ]; then
    java -jar proguard.jar @config.pro -verbose
else
    mv simvis_fat.jar simvis.jar
fi
