usage() {
    echo   ""
    echo   "    Usage: $0 <schd-exec> <config-file> <algorithm-id> [-n]"
    echo   "        schd-exec       the file path of the scheduler executable."
    echo   "        config-file     the configuration file to run the simulator with."
    echo   "        algorithm-id    the ID of the algorithm to run the scheduler with."
    echo   ""
    exit
}

basePath="$(pwd)"

if [ "$1" == "" ]; then usage
elif [ "$1" == "-h" ]; then usage
elif [ ! -f $1 ]; then
    echo "File $1 does not exist."
    exit
fi

schdExec="$1"

if [ "$2" == "" ]; then usage
elif [ ! -f $2 ]; then usage
fi

configFile="$2"

if [ "$3" == "" ]; then usage
fi

algorithmId="$3"

useNewLineMode=0

if [ "$4" == "-n" ]; then useNewLineMode=1
elif [ "$4" != "" ]; then exit
fi

dirSchd="$(cd $(dirname $schdExec); pwd)"
dirConf="$(cd $(dirname $configFile); pwd)"
fileSchd="$(basename $schdExec)"
fileConf="$(basename $configFile)"

trap "kill 0" EXIT

cd "$dirSchd"

sleep 1
if [ $useNewLineMode -eq 1 ]
    then ./ds-server -c "$dirConf/$fileConf" -v brief -n > temp.log&
    else ./ds-server -c "$dirConf/$fileConf" -v brief > temp.log&
fi
sleep 1
if [ "${fileSchd##*.}" == "class" ]
    then java ${filename%.*} "-a" $algorithmId
    else ./$fileSchd -a $algorithmId
fi
sleep 1

cd "$basePath"

java -jar simvis.jar "$dirSchd/system.xml" "$dirSchd/ds-jobs.xml" "$dirSchd/temp.log" "../app/data.js" "../app/index.html"

# rm "$BIN_PATH/temp.log"
