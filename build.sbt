ThisBuild/organization := "org.bitbucket.nhweston"
ThisBuild/scalaVersion := "2.12.8"
ThisBuild/scalacOptions := Seq (
    "-deprecation",
    "-feature",
    "-unchecked",
    "-Xcheckinit"
)

lazy val root = (project in file(".")).settings (
    name := "comp335-visual",
    version := "0.1",
    libraryDependencies ++= Seq (
        "org.scala-lang.modules" %% "scala-xml" % "1.1.1",
        "org.scala-lang.modules" %% "scala-parser-combinators" % "1.1.2"
    )
) .settings (
    assemblyJarName in assembly := "simvis_fat.jar",
    assemblyOutputPath in assembly := file(".") / "bin" / "simvis_fat.jar",
    mainClass in assembly := Some("org.bitbucket.nhweston.comp335_visual.Main"),
    test in assembly := {}
)
