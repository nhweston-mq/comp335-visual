package org.bitbucket.nhweston.comp335_visual

case class Event (
    time: Int,
    jobId: Int,
    instanceId: Int,
    typeId: String,
    state: String
) {

    lazy val js: String = {
        s"""    {
           |        time: $time,
           |        jobId: $jobId,
           |        instanceId: $instanceId,
           |        typeId: "$typeId",
           |        state: "$state"
           |    }""".stripMargin
    }

}
