package org.bitbucket.nhweston.comp335_visual

case class Job (
    id: Int,
    time: Int,
    estimatedRuntime: Int,
    numCores: Int,
    memory: Int,
    disk: Int
) {

    lazy val js: String = {
        s"""    {
           |        id: $id,
           |        time: $time,
           |        estimatedRuntime: $estimatedRuntime,
           |        numCores: $numCores,
           |        memory: $memory,
           |        disk: $disk
           |    }""".stripMargin
    }

}
