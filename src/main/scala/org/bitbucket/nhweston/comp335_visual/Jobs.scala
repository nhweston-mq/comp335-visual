package org.bitbucket.nhweston.comp335_visual

import scala.xml.XML

case class Jobs (jobs: Seq[Job]) {

    lazy val js: String = {
        """const DATA_JOBS = [""" + "\n" + jobs.map (_.js) .mkString (",\n") + "\n" + """];"""
    }

}

object Jobs {

    def apply (path: String) : Jobs = Jobs {
        (XML.load (path) \ "job") .map {
            n => Job (
                (n \@ "id") .toInt,
                (n \@ "submitTime") .toInt,
                (n \@ "estRunTime") .toInt,
                (n \@ "cores") .toInt,
                (n \@ "memory") .toInt,
                (n \@ "disk") .toInt
            )
        }
    }

}
