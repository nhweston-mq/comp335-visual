package org.bitbucket.nhweston.comp335_visual

import scala.io.Source

case class Log (events: Seq[Event]) {

    lazy val js: String = {
        """const DATA_LOG = [""" + "\n" + events.map (_.js) .mkString (",\n") + "\n" + """];"""
    }

}

object Log {

    def apply (path: String) : Log = {
        new LogParser () (
            Source.fromFile (path) .getLines .toSeq .collect {
                case line if line startsWith "t:" => line drop 2
            }
        )
    }

}
