package org.bitbucket.nhweston.comp335_visual

import scala.util.parsing.combinator.{ImplicitConversions, RegexParsers}

class LogParser extends RegexParsers with ImplicitConversions {

    def apply (input: Seq[String]) : Log = {
        Log {
            input.map {
                parseAll (event, _) match {
                    case Success (result, _) => result
                    case failure: NoSuccess => throw new IllegalArgumentException (failure.msg)
                }
            }
        }
    }

    lazy val event: Parser[Event] = {
        jobTime ~ jobId ~ instanceId ~ typeId ~ jobState ^^ Event
    }

    lazy val jobTime: Parser[Int] = int
    lazy val jobId: Parser[Int] = "job" ~> int <~ """\([a-z]*\)""".r.?
    lazy val instanceId: Parser[Int] = "on" ~> """#""" ~> int
    lazy val typeId: Parser[String] = "of" ~> "server" ~> identifier <~ """\([a-z]*\)""".r.?
    lazy val jobState: Parser[String] = """[A-Z]+""".r

    lazy val identifier: Parser[String] = {
        """[a-zA-Z0-9_-]*""".r
    }

    lazy val int: Parser[Int] = {
        """[0-9]+""".r ^^ (_.toInt)
    }

}
