package org.bitbucket.nhweston.comp335_visual

import java.io.FileWriter

import scala.xml.PrettyPrinter

object Main {

    def main (args: Array[String]) : Unit = {
        args.toSeq match {
            case Seq (pathConfig, pathJobs, pathLog, pathData, pathHTML) =>
                val config = System (pathConfig)
                val jobs: String = Jobs (pathJobs) .js
                val log: String = Log (pathLog) .js
                val text: String = """"use strict";""" + "\n\n" + config.js + "\n\n" + jobs + "\n\n" + log
                val dataWriter = new FileWriter (pathData)
                dataWriter.write (text)
                dataWriter.close ()
                val htmlWriter = new FileWriter (pathHTML)
                htmlWriter.write (new PrettyPrinter (256, 4) .format (config.html))
                htmlWriter.close ()
            case _ => println ("Usage: <this-program> <config-path> <jobs-path> <log-path> <data-path> <html-path>")
        }
    }

}
