package org.bitbucket.nhweston.comp335_visual

import org.bitbucket.nhweston.comp335_visual.XMLUtil.<>

import scala.xml.{Node, Text}

case class ServerType (
    id: String,
    limit: Int,
    bootupTime: Int,
    hourlyRate: BigDecimal,
    numCores: Int,
    memory: Int,
    disk: Int
) {

    val LEN_BAR: Int = 160
    val MB_PER_PX: Int = 100
    val MB_PER_BAR: Int = 100 * 160

    lazy val numFullMemBars: Int = memory / MB_PER_BAR
    lazy val lastMemBarLen: Int = memory % MB_PER_BAR

    lazy val numFullDiskBars: Int = disk / (MB_PER_BAR * 4)
    lazy val lastDiskBarLen: Int = disk % (MB_PER_BAR * 4)

    lazy val js: String = {
        s"""    {
           |        id: "$id",
           |        limit: $limit,
           |        bootupTime: $bootupTime,
           |        hourlyRate: $hourlyRate,
           |        numCores: $numCores,
           |        memory: $memory,
           |        disk: $disk
           |    }""".stripMargin
    }

    lazy val html: Node = {
        <> (
            "div",
            "class" -> "type-container",
            "id" -> s"type__$id"
        ) (
            (0 until limit) .map {
                i => <> (
                    "div",
                    "class" -> "instance-container",
                    "id" -> s"instance__${id}__$i"
                ) (
                    <> ("div") (Text (s"$id $i")),
                    <> ("span") (Text (s"Cores:")),
                    <> ("div", "class" -> "cores") (
                        (0 until numCores) .map {
                            j => <> (
                                "div",
                                "class" -> "core-icon",
                                "id" -> s"core__${id}__${i}__$j"
                            ) ()
                        } :_*
                    ),
                    <> ("span") (Text ("Memory:")),
                    <> ("div", "class" -> "memory") (
                        (0 until numFullMemBars) .map {
                            j => <> (
                                "div",
                                "class" -> "mem-bar",
                                "id" -> s"mem__${id}__${i}__$j"
                            ) ()
                        } ++ (
                            if (lastMemBarLen == 0) None
                            else Some (
                                <> (
                                    "div",
                                    "class" -> "mem-bar-trunc",
                                    "id" -> s"mem__${id}__${i}__$numFullMemBars",
                                    "style" -> s"width: ${lastMemBarLen / MB_PER_PX}px"
                                ) ()
                            )
                        ) .toSeq :_*
                    ),
                    <> ("span") (Text ("Disk:")),
                    <> ("div", "class" -> "disk") (
                        (0 until numFullDiskBars) .map {
                            j => <> (
                                "div",
                                "class" -> "disk-bar",
                                "id" -> s"disk__${id}__${i}__$j"
                            ) ()
                        } ++ (
                            if (lastDiskBarLen == 0) None
                            else Some (
                                <> (
                                    "div",
                                    "class" -> "disk-bar-trunc",
                                    "id" -> s"disk__${id}__${i}__$numFullMemBars",
                                    "style" -> s"width: ${lastMemBarLen / MB_PER_PX}px"
                                ) ()
                            )
                            ) .toSeq :_*
                    ),
                    <> ("div") (
                        <span>Active:</span>,
                        <> (
                            "span",
                            "class" -> "active",
                            "id" -> s"active__${id}__$i"
                        ) ()
                    ),
                    <> ("div") (Text ("Queue:")),
                    <> (
                        "div",
                        "class" -> "queue",
                        "id" -> s"queue__${id}__$i"
                    ) ()
                )
            } :_*
        )
    }

}
