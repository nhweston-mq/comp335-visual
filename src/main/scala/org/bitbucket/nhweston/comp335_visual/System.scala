package org.bitbucket.nhweston.comp335_visual

import org.bitbucket.nhweston.comp335_visual.XMLUtil.<>

import scala.xml.{Node, XML}

case class System (types: Seq[ServerType]) {

    lazy val js: String = {
        """const DATA_SYSTEM = [""" + "\n" + types.map (_.js) .mkString (",\n") + "\n" + """];"""
    }

    lazy val html: Node = {
        <html lang="en">
            <head>
                <meta charset="UTF-8" />
                <link rel="stylesheet" href="styles.css"/>
                <script src="data.js"></script>
                <script src="colors.js"></script>
                <script src="job.js"></script>
                <script src="server.js"></script>
                <script src="index.js"></script>
                <title>Simulator Visualisation</title>
            </head>
            <body>
                <div class="controls">
                    <div id="controls-left">
                        <div id="info">&nbsp;</div>
                        <div id="step-inputs">
                            <button class="step-button" onclick="prev(100);">100</button>
                            <button class="step-button" onclick="prev(10);">10</button>
                            <button class="step-button" onclick="prev(1);">«</button>
                            <input id="step-field" class="step-field" type="number" value="-1" onchange="setStep()"/>
                            <button class="step-button" onclick="next(1);">»</button>
                            <button class="step-button" onclick="next(10);">10</button>
                            <button class="step-button" onclick="next(100);">100</button>
                        </div>
                    </div>
                    <div id="controls-right">
                        <span>Speed:</span><input id="animate-speed" type="number"/>
                        <form>
                            <input type="radio" id="animate-by-step" name="animation-type" checked=""/><span> By Step</span>
                            <input type="radio" id="animate-by-time" name="animation-type"/><span> By Time</span>
                        </form>
                        <button onclick="startAnimation()">Start</button>
                        <button onclick="stopAnimation()">Stop</button>
                    </div>
                </div>
                <div class="content">{types.map (_.html)}</div>
            </body>
        </html>
    }

}

object System {

    def apply (path: String) : System = System {
        (XML.loadFile (path) \ "servers" \ "server") .map {
            n => ServerType (
                n \@ "type",
                (n \@ "limit") .toInt,
                (n \@ "bootupTime") .toInt,
                BigDecimal (n \@ "rate"),
                (n \@ "coreCount") .toInt,
                (n \@ "memory") .toInt,
                (n \@ "disk") .toInt
            )
        }
    }

}
