package org.bitbucket.nhweston.comp335_visual

import scala.xml._

object XMLUtil {

    def <> (label: String, attributes: (String, String)*) (children: Node*) : Node = {
        Elem (
            null,
            label,
            attributes.foldRight[MetaData] (Null) {
                case ((k, v), md) => Attribute (k, Seq (Text (v)), md)
            },
            TopScope,
            false,
            children :_*
        )
    }

}
